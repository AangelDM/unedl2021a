﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormaCaptura : Form
    {
        string[,] personas = new string[,]
        { { "Angel","Duran" },
          { "Christopher","Villalobos" },
          { "Daniel","Lopez" },
          { "Daniel","Vazquez" },
          { "Edgar","Banuelos" },
          { "Fernando","Hernandez" },
          { "Francisco","Garcia" },
          { "Ivan","Narvaez" },
          { "Joel","Juarez" },
          { "Juan","Romero" },
          { "Kevin","Gonzalez" },
          { "Luis","Gomez" },
          { "Manuel","Mariscal" },
          { "Mario","Mercado" },
          { "Marisol","Benitez" },
          { "Mauricio","Castaneda" },
          { "Oscar","Ochoa" },
          { "Yahayra","Rodriguez" },
          { "Cesar", "De la Cruz" }
        };

        public FormaCaptura()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int r = 0;
            String nombre = txtBuscaNombre.Text;
            String apellido = txtBuscaApellido.Text;
            for (int i = 0; i < 19; i++)
            {
                for(int y = 0; y <= 1; y++)
                {
                    if (nombre == personas[i, y])
                    {
                        MessageBox.Show("Encontrado: "+ personas[i,0] + " " + personas[i,1], "Nombre encontrado");
                        r = 1;
                        break;
                    }
                    if (apellido == personas[i, y])
                    {
                        MessageBox.Show("Encontrado: " + personas[i, 0] + " " + personas[i, 1], "Apellido encontrado");
                        r = 1;
                        break;
                    }
                }
            }
            if (r == 0)
            {
                MessageBox.Show("No encontrado");
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtBuscaApellido.Text = "";
            txtBuscaNombre.Text = "";
        }
    }
}
