﻿using System;

namespace TutorialLINQ
{
    class Program
    {
        private static object students;

        static void Main(string[] args)
        {
            // Create the query.
            // The first line could also be written as "var studentQuery ="
            System.Collections.Generic.IEnumerable<Student> studentQuery =
                from student in students
                where student.Scores[0] > 90
                select student;

            // Execute the query.
            // var could be used here also.
            foreach (Student student in studentQuery)
            {
                Console.WriteLine("{0}, {1}", student.Last, student.First);
            }



            // studentQuery2 is an IEnumerable<IGrouping<char, Student>>
            var studentQuery2 =
                from student in students
                group student by student.Last[0];
        }
    }
}
