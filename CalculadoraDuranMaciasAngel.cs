﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblResultado_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblR.Text = "  ";
            textBox1.Text = "0";
            textBox2.Text = "0";
        }

        private void buttonSuma_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(textBox1.Text);
            double b = Convert.ToDouble(textBox2.Text);
            double c = a+b;
            lblR.Text = c.ToString() ;
        }

        private void buttonLimp_Click(object sender, EventArgs e)
        {
            lblR.Text = "  ";
            textBox1.Text = "0";
            textBox2.Text = "0";
        }

        private void buttonResta_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(textBox1.Text);
            double b = Convert.ToDouble(textBox2.Text);
            double c = a - b;
            lblR.Text = c.ToString();
        }

        private void buttonMulti_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(textBox1.Text);
            double b = Convert.ToDouble(textBox2.Text);
            double c = a * b;
            lblR.Text = c.ToString();
        }

        private void buttonDiv_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(textBox1.Text);
            double b = Convert.ToDouble(textBox2.Text);
            if (b == 0)
            {
                lblR.Text = "No se puede dividir entre 0";
            }
            if(a < 0 && b < 0)
            {
                lblR.Text = "Error: Expresion incompleta";
            }
            else
            {
                double c = a / b;
                lblR.Text = c.ToString();
            }
        }
    }
}
